import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    credentials: null,
    users: [ {id: 0, name: 'Hard Coded'}]
  },
  getters: {
    credentialUsername(state, getters) {
      return state.credentials != null ? state.credentials.username : ''
    },
    credentialPassword(state, getters) {
      return state.credentials != null ? state.credentials.password : ''
    },
    getUserById: (state) => (id) => {
      return state.users.find(user => user.id === id)
    }
  },
  actions: {
    saveCredentials({commit}, credentials) {
      commit('setCredentials', credentials)
    },
    fetchUserWithId(context, user_id) {
      return new Promise((resolve, reject) => {
        let user = context.state.users.find(user => user.id === user_id)
        if (user == null) {
          context.dispatch('apichangelater', user_id)
        }
      })
    },
    apichangelater(context, user_id) {
      return new Promise((resolve, reject) => {
        console.log('using axios now')
        axios({
          url: 'https://cors-anywhere.herokuapp.com/https://datalex.testrail.net/index.php?/api/v2/get_user/' + user_id,
          method: 'get',
          auth: {
            username: context.getters.credentialUsername,
            password: context.getters.credentialPassword
          },
          params: {

          },
          headers: {
            'Content-Type': 'application/json',
          },
          data: {}
          })
          .then((response) => {
            context.commit('pushUser', response.data)
          })
          .catch((error) => {

          })
          .then(() => {})
      })
    }
  },
  mutations: {
    setCredentials(state, credentials) {
      state.credentials = credentials
    },
    pushUser(state, user) {
      state.users.push(user)
    }
  }
})
